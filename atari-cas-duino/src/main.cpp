#include <Arduino.h>
#include <SD.h>
#define SD_ChipSelectPin 10 // corresponde al puerto D10 del arduino
#include <TMRpcm.h>
#include <SPI.h>
#include <WString.h>

#define LED 14         // corresponde al puerto A0 del arduino
#define SPEAKER 9      // corresponde al puerto D9 del arduino
#define NUM_SWITCHES 7 // Cantidad de posiciones utilizadas del dip switch

TMRpcm audio;

int fileNum;

// Este arreglo indica los puertos a los cuales está
// conectado cada posición del dip switch, desde
// la posición 8 hasta la 2. La posición 1 se omite,
// ya que no es utilizada.
uint8_t dip_switches[NUM_SWITCHES] = {
    8, // corresponde al puerto D8 del arduino
    7, // corresponde al puerto D7 del arduino
    6, // corresponde al puerto D6 del arduino
    5, // corresponde al puerto D5 del arduino
    4, // corresponde al puerto D4 del arduino
    3, // corresponde al puerto D3 del arduino
    2  // corresponde al puerto D2 del arduino
};

uint16_t read_bit(int i)
{
    uint16_t bit = digitalRead(dip_switches[i]);
    return ~bit & 0x0001;
}

/**
 * Lee el estado de las posiciones del dip switch
 * y obtiene el número del archivo a leer desde
 * la SD-card
 */
uint16_t getFileNumber()
{
    for (size_t i = 0; i < NUM_SWITCHES; i++)
    {
        pinMode(dip_switches[i], INPUT_PULLUP);
    }
    uint16_t number = 0;

    for (size_t i = 0; i < NUM_SWITCHES; i++)
    {
        Serial.end();
        uint16_t bit = read_bit(i);
        Serial.begin(9600);
        Serial.print(bit);
        number |= bit << i;
    }
    Serial.println();
    return number;
}

/**
 * Obtiene el nombre del archivo que corresponde al 
 * número seleccionado con el dip switch
 */
String selectFile(File dir, int fileNumber)
{

    String name;
    int i = 0;
    File f;
    dir.rewindDirectory();
    do
    {
        f = dir.openNextFile();
        if (!f)
        {
            dir.rewindDirectory();
            f = dir.openNextFile();
        }
        name = String(f.name());
        Serial.println(name);
        if (!f.isDirectory() && name.endsWith(".WAV"))
        {
            i++;
        }
        f.close();
    } while (i < fileNumber);

    return String(dir.name()) + "/" + name;
}

/**
 * Reproduce el archivo correspondiente al número seleccionado en el dip switch.
 * Para dar tiempo al usuario de presionar PLAY y ENTER en el computador, la 
 * reproducción comienza automáticamente 5 segundos después de ser encendido el computador.
 */
void playFile(int fileNum)
{
    Serial.print("Seeking file #");
    Serial.println(fileNum);
    String filename = selectFile(SD.open("/WAVS"), fileNum);
    Serial.print("Found: ");
    Serial.println(filename);
    Serial.print("Waiting for 5 seconds...");
    digitalWrite(LED, HIGH);
    delay(500);
    digitalWrite(LED, LOW);
    delay(4000);
    digitalWrite(LED, HIGH);
    delay(500);
    digitalWrite(LED, LOW);
    Serial.print("Playing: ");
    Serial.println(filename.c_str());
    audio.play((char *)filename.c_str());
}

/**
 * 
 */
void setup()
{

    audio.speakerPin = SPEAKER;
    pinMode(LED, OUTPUT);
    Serial.begin(9600);
    if (!SD.begin(SD_ChipSelectPin))
    {
        Serial.println("SD Fail");
    }
    else
    {
        Serial.println("SD OK");
    }

    fileNum = getFileNumber();
    playFile(fileNum);
}

void loop()

{
}