# Atari-CAS-DUINO

Atari-CAS-DUINO es un cassette que permite seleccionar hasta 128 juegos almacenados en una tarjeta SD y cargarlos en un Atari 800XL, 65XE o similar.



## Materiales

* Una placa [Arduino Nano o compatible](https://wiki.eprolabs.com/index.php?title=Arduino_Nano)
* [Placa de prototipado de al menos 4x10 posiciones](https://en.wikipedia.org/wiki/Stripboard)
* [Dip switch de 8 posiciones](https://es.wikipedia.org/wiki/Interruptor_DIP)
* [Cabezal de reproductor de cassettes](https://en.wikipedia.org/wiki/Tape_head)
* [Módulo de tarjeta MicroSD](https://wiki.eprolabs.com/index.php?title=SD_Card_Module)
* [Tarjeta MicroSD](https://en.wikipedia.org/wiki/SD_card#Micro-cards) con suficiente capacidad para guardar los archivos de audio
* Un [cassette](https://en.wikipedia.org/wiki/Cassette_tape) sin cinta
* 2 [Terminales eléctricos de anillo](https://en.wikipedia.org/wiki/Electrical_connector#/media/File:Ring_wire_end_connector.jpg) o similares. Incluso una pequeña placa de metal puede servir
* Cables varios


## Diseño físico
Antes de armar el dispositivo hay que habrir el cassette, sacarle la cinta y hacer una muesca como se muestra en la figura:

![](doc/cassette1.jpg)

Esta muesca sirve para poder colocar la placa Arduino Nano o compatible dentro del cassette.

La siguiente foto muestra los demás agujeros a perforar. El Dip Switch requiere un agujero rectangular para que los switches sean visibles para el usuario. De forma similar el led también requiere un agujero circular para que sea visible. También se recomienda hacer un agujero para dejar expuesto el botón de RESET de la placa Arduino.

![](doc/cassette2.jpg)

Para acceder a la tarjeta MicroSD desde fuera del cassette, es necesario hacer muescas en el lado del cassette como se muestran a continuación:

![](doc/cassette4.jpg)

La siguiente foto muestra dónde deberían colocarse los componentes dentro del cassette. Los componentes los pegué al cassette utilizando silicona caliente.


![](doc/atari-cas-duino_fisico.jpg)


La placa de prototipado en la parte superior izquierda tiene una pista para la conexión a +5v (en la parte superior) y otra pista para conectar a GND (en la parte inferior). El terminal eléctrico de anillo (de color dorado en la parte superior izquierda) se conecta a la pista de +5v.

El led con la resistencia de 2k pueden colocarse en cualquier lugar donde quepa. En la foto está entre la placa Arduino y el Dip Switch. 

El Dip Switch está a la derecha de la placa Arduino. 

El cabezal de cassette va en la parte inferior. Debe colocarse de tal forma que quede lo más cerca posible del cabezal de la cassettera cuando se oprime PLAY.


![](doc/cassette3.jpg)


## Modificaciones a la cassettera
Para alimentar el cassette es necesario colocar un terminal eléctrico o una simple placa de metal en donde se muestra en la foto (arriba a la derecha). Este terminal debe ir conectado por un cable a los +5v de la cassettera


![](doc/cassettera1.jpg)


Es importante que el cassette haga buen contacto con la cassettera en dos lugares:

* El puerto USB de la placa Arduino debe hacer contacto con la placa metálica que sostiene el cassette en su lugar. Esto permite conectar el cassette a GND.
* El terminal eléctrico de anillo del cassette debe hacer contacto con el terminal de la cassettera que fue conectado a los +5v

![](doc/cassettera2.jpg)



## Esquema electrónico
La siguiente imagen indica cómo se conectan los diferentes componentes.


![](doc/atari-cas-duino_schem.png)


## Preparación de la tarjeta MicroSD

La tarjeta MicroSD debe estar formateada en formato FAT32. Debe tener una carpeta llamada WAVS donde deben colocarse los archivos de audio con los programas a cargar. Los archivos de audio deben ser de tipo WAV (8-bit PCM, mono, 11025 Hz, 88 bps).


## Código fuente

El código está en la carpeta [atari-cas-duino](atari-cas-duino). Este código requiere instalar [PlatformIO](https://platformio.org/). Para compilar y subir el código a la placa Arduino, se pueden seguir las instrucciones de este [tutorial](https://www.luisllamas.es/programar-arduino-con-atom-y-platformio-ide/). Lo único diferente es que el código a subir es el que se encuentra en [main.cpp](atari-cas-duino/src/main.cpp) en lugar de el que aparece en el tutorial.

El archivo [main.cpp](atari-cas-duino/src/main.cpp) implementa la lectura de archivos de audio desde una tarjeta SD y su reproducción en el cabezal del cassette. Dicho archivo tiene varios comentarios para facilitar su modificación o adaptación para otros usos.

## Dudas o aclaraciones

Cualquier duda pueden preguntar en mi canal, en los comentarios del [video demostrativo](https://www.youtube.com/watch?v=7kK1SHDgiAE) 